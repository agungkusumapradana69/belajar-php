<?php
$nama_produk1="Lenovo";
$harga_produk1=8000000;
$deskripsi_produk1="Intel Core i7-1195G7, Quad Core up to 5.0 GHz  16GB  1 TB SSD OLED, 14 Inci UHD+ 3840 x 2160 piksel.";

$nama_produk2="Dell";
$harga_produk2=8500000;
$deskripsi_produk2="Intel Core i5-1230U, 10-Core up to 4.4 GHz 8GB  512 GB SSDIPS LED, 13.4 Inci FHD+ 1920 x 1200 piksel.";

$nama_produk3="Asus";
$harga_produk3=9000000;
$deskripsi_produk3="Asus Intel Core i5-13500H, 12-Core up to 4.7 GHz (18MB cache) 8GB DDR4 512 GB M.2 NVMe PCIe SSD OLED 14 inci 3K 2880 x 1800 piksel (90Hz).";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <title>Produk</title>
</head>
<body>
  <div class="p-5 mb-4 bg-success-subtle rounded-3">
  <div class="row">
    <div class="col">
      <div class="card">
      <img src="lenovo.png" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title"><?php echo $nama_produk1;?></h5>
        <p class="card-text"><?php echo $deskripsi_produk1;?> </p>
        <p class="card-text"><?php echo $harga_produk1;?></p>
        <a href="#" class="btn btn-primary w-100">Go somewhere</a>
      </div>
      </div>
    </div>
    <div class="col">
      <div class="card">
      <img src="dell.jpg" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title"><?php echo $nama_produk2;?></h5>
        <p class="card-text"><?php echo $deskripsi_produk2;?></p>
        <p class="card-text"><?php echo $harga_produk2;?></p>
        <a href="#" class="btn btn-primary w-100">Go somewhere</a>
      </div>
      </div>
    </div>
    <div class="col">
      <div class="card">
      <img src="asus.jpg" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title"><?php echo $nama_produk3;?></h5>
        <p class="card-text"><?php echo $deskripsi_produk3;?></p>
        <p class="card-text"><?php echo $harga_produk3;?></p>
        <a href="#" class="btn btn-primary w-100">Go somewhere</a>
      </div>
      </div>
    </div>
    
  </div>
</body>
</html>